package com.kleber.client;

import com.kleber.teste.CustomerPort;
import com.kleber.teste.CustomerPortService;
import com.kleber.teste.GetCustomerDetailRequest;
import com.kleber.teste.GetCustomerDetailResponse;

public class ClientTest {

	
	public static void main(String[] args) {
		CustomerPortService service = new CustomerPortService();
		HeaderHandlerResolver resolver = new HeaderHandlerResolver();
		service.setHandlerResolver(resolver);
		CustomerPort port = service.getCustomerPortSoap11();
		
		GetCustomerDetailRequest req = new GetCustomerDetailRequest();
		req.setId(3);
		
		GetCustomerDetailResponse resp = port.getCustomerDetail(req);
		
		System.out.println("id: " + resp.getCustomerDetail().getId());
		System.out.println("name: " + resp.getCustomerDetail().getName());
		System.out.println("email: " + resp.getCustomerDetail().getEmail());
		System.out.println("phone: " + resp.getCustomerDetail().getPhone());
		
	}
}
